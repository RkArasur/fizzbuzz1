﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models
{
    public class FizzBuzzViewModel
    {
        [Required]
        [Range(1,1000)]
        public int number { get; set; }
    }
}