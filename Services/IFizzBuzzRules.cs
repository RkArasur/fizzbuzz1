﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;

namespace WebApplication2.Services
{
    public interface IFizzBuzzRules
    {
        bool Evaluate(int val);
        string GetResult(DateTime now);
    }
}
