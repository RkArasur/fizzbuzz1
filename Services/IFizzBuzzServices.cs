﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication2.Services
{
    public interface IFizzBuzzServices
    {
        List<string> SolveFizzBuzz(int val);
    }
}
