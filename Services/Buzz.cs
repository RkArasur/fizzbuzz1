﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Services
{
    public class Buzz:IFizzBuzzRules
    {
        public bool Evaluate(int x)
        {
            return (x % 5 == 0);
        }

        public string GetResult(DateTime now)
        {
            if (now.DayOfWeek != DayOfWeek.Wednesday)
            {
                return "buzz";
            }
            else
            {
                return "wuzz";
            }
        }
    }
}