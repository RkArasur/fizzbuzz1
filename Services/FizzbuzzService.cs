﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap;


namespace WebApplication2.Services
{
    public class FizzbuzzService:IFizzBuzzServices 
    {
        private IList<IFizzBuzzRules> rules;
        
        public FizzbuzzService(IList<IFizzBuzzRules> rules)
        {
            this.rules = rules;
        }

        public List<string> SolveFizzBuzz(int val)
        {
            if(val <= 1 || val >= 1000)
            {
                throw new ArgumentException("NUmber should be within the range of 1-1000 (inclusive)");
            }
            List<string> FizzBuzzResult = new List<string>();
            for(int i = 1; i <= val; i++)
            {
                var result = this.rules.Where(e => e.Evaluate(i));
                string res = "";
                foreach (var item in result)
                {
                    res += item.GetResult(DateTime.UtcNow);
                }
                if(res == "")
                {
                    FizzBuzzResult.Add(i.ToString());
                }
                else
                {
                    FizzBuzzResult.Add(res);
                }
            }
            return FizzBuzzResult;
        }
    }
}