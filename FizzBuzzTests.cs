using NUnit.Framework;
using System;
using System.Collections.Generic;
using WebApplication2.Controllers;
using WebApplication2.Services;
using System.Web.Mvc;

namespace WebApplication2Test
{
    public class Tests
    {

        Buzz bz;
        Fizz fz;
        private readonly DateTime notWednesday = new DateTime(2021, 11, 13);
        private readonly DateTime wednesday = new DateTime(2021, 11, 10);

        public class DummyService : IFizzBuzzServices
        {
            public List<string> SolveFizzBuzz(int val)
            {
                List<string> l = new List<string>();
                return l;
            }
        }
        [SetUp]
        public void Setup()
        {
            bz = new Buzz();
            fz = new Fizz();
        }

        [Test]
        public void BuzzEvaluateFalse()
        {
            Assert.AreEqual(false, bz.Evaluate(3));
        }
        [Test]
        public void BuzzEvaluateTrue()
        {
            Assert.AreEqual(true, bz.Evaluate(5));
        }


        [Test]
        public void FizzEvaluateFalse()
        {
            Assert.AreEqual(false, fz.Evaluate(10));
        }
        [Test]
        public void FizzEvaluateTrue()
        {
            Assert.AreEqual(true, fz.Evaluate(6));
        }


        [Test]
        public void FizzgetResult_Wednesday()
        {
            Assert.AreEqual("wizz", fz.GetResult(wednesday));
        }
        [Test]
        public void BuzzgetResult_Wednesday()
        {
            Assert.AreEqual("wuzz", bz.GetResult(wednesday));
        }


        [Test]
        public void FizzGetResult_Not_Wednesday()
        {
            Assert.AreEqual("fizz", fz.GetResult(notWednesday));
        }
        [Test]
        public void BuzzGetResult_Not_Wednesday()
        {
            Assert.AreEqual("buzz", bz.GetResult(notWednesday));
        }


        [Test]
        public void Cntroller_Index_Page_Test()
        {
            var controller = new FizzBuzzController(new DummyService());
            var result = (ViewResult)controller.Index();
            Assert.AreEqual("Index", result.ViewName);
        }

        [Test]
        public void Solve_FizzBuzz_Number_Greater_Than_thousand()
        {
            IList<IFizzBuzzRules> l = new List<IFizzBuzzRules>();
            l.Add(new Buzz());
            l.Add(new Fizz());
            var service = new FizzbuzzService(l);
            Assert.Throws<ArgumentException>(() => service.SolveFizzBuzz(4235));
        }
        [Test]
        public void Solve_FizzBuzz_Number_Less_Than_One()
        {
            IList<IFizzBuzzRules> l = new List<IFizzBuzzRules>();
            l.Add(new Buzz());
            l.Add(new Fizz());
            var service = new FizzbuzzService(l);
            Assert.Throws<ArgumentException>(() => service.SolveFizzBuzz(-89));
        }

        [Test]
        public void Solve_FizzBuzz_Valid_Number()
        {
            IList<IFizzBuzzRules> l = new List<IFizzBuzzRules>();
            l.Add(new Buzz());
            l.Add(new Fizz());
            var service = new FizzbuzzService(l);
            List<string> expected = new List<string>();
            expected.Add("1");
            expected.Add("2");
            expected.Add("fizz");
            expected.Add("4");
            expected.Add("buzz");
            expected.Add("fizz");
            Assert.AreEqual(expected, service.SolveFizzBuzz(6));
        }
    }
}