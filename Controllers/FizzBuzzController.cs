﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Services;
using WebApplication2.Models;
using StructureMap;

namespace WebApplication2.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzServices _services;

        public FizzBuzzController(IFizzBuzzServices services)
        { 
            _services = services;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetFizzBuzz(FizzBuzzViewModel FzModel)
        {
            ViewBag.res = _services.SolveFizzBuzz(FzModel.number);
            return View();
        }
    }
}